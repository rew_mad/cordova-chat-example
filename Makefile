build:
	@go build -o cordova-chat-example main.go

run:
	@go run main.go

gomobile:
	@gomobile bind -target android -o ./cordova-plugin-chat-example/native/lib.aar ./server/

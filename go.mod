module gitlab.com/rew_mad/cordova-chat-example

go 1.12

require (
	github.com/gorilla/websocket v1.4.1
	golang.org/x/mobile v0.0.0-20200212152714-2b26a4705d24 // indirect
)

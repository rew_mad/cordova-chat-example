export const environment = {
  production: true,
  wsUrl: '',
  avatarsUrl: 'https://api.adorable.io/avatars/285',
  cordovaEnabled: false
};

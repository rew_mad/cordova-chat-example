import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { DialogUserType } from './dialog-user-type';

export interface DialogUserParams {
  username?: string;
  title: string;
}

export interface DialogUserResult {
  username: string;
  dialogType: DialogUserType;
  previousUsername: string;
}

@Component({
  selector: 'app-dialog-user',
  templateUrl: './dialog-user.component.html',
  styleUrls: ['./dialog-user.component.css']
})
export class DialogUserComponent {
  usernameFormControl = new FormControl('', [Validators.required]);
  previousUsername: string;

  constructor(public dialogRef: MatDialogRef<DialogUserComponent, DialogUserResult>,
              @Inject(MAT_DIALOG_DATA) public params: DialogUserParams,
              private translate: TranslateService) {
    this.previousUsername = params.username ? params.username : undefined;
    this.usernameFormControl.setValue(params.username);
    translate.setDefaultLang('en');
  }

  public onSave(createServer: boolean): void {
    this.dialogRef.close({
      username: this.usernameFormControl.value,
      dialogType: createServer ? DialogUserType.NEW : DialogUserType.EXISTED,
      previousUsername: this.previousUsername
    });
  }
}

import { Injectable } from '@angular/core';

@Injectable()
export class ChatServerService {

    start(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            chat.start(() => resolve(), err => reject(err), 8080);
        });
    }

    stop(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            chat.stop(() => resolve(), err => reject(err));
        });
    }
}

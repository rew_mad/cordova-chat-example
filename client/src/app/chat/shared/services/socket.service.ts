import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WebSocketSubject } from 'rxjs/webSocket';

import { environment } from '../../../../environments/environment';
import { Message } from '../model/message';
import { Action } from '../model/action';

interface WSMessage {
    type: 'join' | 'leave' | 'text';
    value: string;
    author?: string;
}

function type2Action(type: 'join' | 'leave' | 'text'): Action | undefined {
    switch (type) {
        case 'join':
            return Action.JOINED;

        case 'leave':
            return Action.LEFT;
    }

    return undefined;
}

function name2ID(name: string): number {
    return name.split('').map(x => x.charCodeAt(0)).reduce((acc, v) => (acc + v) % 10000);
}

@Injectable()
export class SocketService {
    private ws: WebSocketSubject<WSMessage>;

    initSocket(username: string, address?: string): void {
        const addressWithProtocol = `${location.protocol.startsWith('https') ? 'wss': 'ws'}://${address || environment.wsUrl}/ws`;
        this.ws = new WebSocketSubject(`${addressWithProtocol}?username=${encodeURIComponent(username)}`);
    }

    send(message: string): void {
        this.ws.next({ type: 'text', value: message });
    }

    onMessage(): Observable<Message> {
        return this.ws.pipe(
            map(x => ({
                action: type2Action(x.type),
                content: x.value,
                from: {
                    avatar: `${environment.avatarsUrl}/${name2ID(x.author)}.png`,
                    name: x.author
                }
            }) as Message)
        );
    }
}

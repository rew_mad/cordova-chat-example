import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.translate.use('en');
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }


  get exitAvailable(): boolean {
    return !!navigator['app'];
  }
  exit() {
    if (!!navigator['app']) {
      navigator['app'].exitApp();
    }
  }
}

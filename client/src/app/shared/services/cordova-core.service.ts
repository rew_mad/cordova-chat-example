import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

const CORDOVA_UNAVAILABLE = 'running outside of Cordova';

@Injectable()
export class CordovaCoreService {
  private apiLoadStatus?: Promise<Event>;
  private cordovaLoaded?: Promise<void>;

  constructor() { }

  loadAPI(): Promise<any> {
    if (!this.apiLoadStatus) {

      this.apiLoadStatus = new Promise<Event>((resolve, reject) => {
        if (!environment.cordovaEnabled) {
          reject(CORDOVA_UNAVAILABLE);
        }

        // TODO: generalize all of this?
        if (!document.head) {
          reject('document.head should exist');
          return;
        }

        const script = document.createElement('script');
        script.src = 'cordova.js';
        script.addEventListener('load', resolve, { once: true });
        script.addEventListener('error', reject, { once: true });
        script.addEventListener('abort', reject, { once: true });
        document.head.appendChild(script);
      });
    }

    return this.apiLoadStatus;
  }

  waitCordova(): Promise<void> {
    if (!this.cordovaLoaded) {
      this.cordovaLoaded = this.loadAPI().then(_ =>
        new Promise<void>((resolve, reject) =>
        document.addEventListener('deviceready', (ev) => {
          try {
            // @ts-ignore
            // tslint:disable-next-line: no-unsafe-any
            cordova.exec.setNativeToJsBridgeMode(cordova.exec.nativeToJsModes.ONLINE_EVENT);
          } catch (err) {
            console.warn('Failed to enable callback keep order mode:', err);
          }
          resolve();
         }, false)
        )
      );
    }

    return this.cordovaLoaded;
  }
}

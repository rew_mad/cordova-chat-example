import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatModule } from './chat/chat.module';
import { SharedModule } from './shared/shared.module';


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { MatMenuModule } from '@angular/material/menu';
import { CordovaCoreService } from './shared/services/cordova-core.service';
import { environment } from '../environments/environment';

export function HttpLoaderFactory(http: HttpClient) {
  const prefix = '/assets/i18n/';
  return new TranslateHttpLoader(http, environment.cordovaEnabled ? '.' + prefix : prefix);
}

export function loadCordova(cordovaCore: CordovaCoreService) {
  return () => {
    cordovaCore.loadAPI().catch(err => console.error('Failed to load Cordov API:', err));
  };
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ChatModule,
    SharedModule,
    HttpClientModule,
    MatMenuModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    CordovaCoreService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadCordova,
      deps: [CordovaCoreService],
      multi: true
    }  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

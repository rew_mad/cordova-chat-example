package server

import "encoding/json"

type stringMessage struct {
	Type   string `json:"type"`
	Value  string `json:"value,omitempty"`
	Author string `json:"author,omitempty"`
}

func newJoinMsg(who string) []byte {
	msg := stringMessage{Type: "join", Author: who}
	bytes, _ := json.Marshal(msg)
	return bytes
}

func newLeaveMsg(who string) []byte {
	msg := stringMessage{Type: "leave", Author: who}
	bytes, _ := json.Marshal(msg)
	return bytes
}

func newTextMsg(author, text string) []byte {
	msg := stringMessage{Type: "text", Value: text, Author: author}
	bytes, _ := json.Marshal(msg)
	return bytes
}

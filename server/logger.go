package server

type logWriter struct {
	observer LogObserver
}

func (w *logWriter) Write(buf []byte) (int, error) {
	if err := w.observer.Next(string(buf)); err != nil {
		return 0, err
	}
	return len(buf), nil
}

package server

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
)

var server *http.Server

// LogObserver provides interface to listen log entries
type LogObserver interface {
	Next(message string) error
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "./client/home.html")
}

// Start runs new server instance listening connections on port and
// optionally serving dist files on / and allowing to observe log entries via custom observer.
// Blocking by default, errors if server already started.
func Start(port int, serveDist bool, logObserver LogObserver) error {
	if server != nil {
		return errors.New("already started")
	}

	if logObserver != nil {
		log.SetOutput(&logWriter{logObserver})
	}

	hub := newHub()
	defer hub.stop()
	go hub.run()

	mux := http.NewServeMux()
	if serveDist {
		mux.HandleFunc("/", serveHome)
	}
	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	addr := fmt.Sprintf(":%d", port)
	server = &http.Server{Addr: addr, Handler: mux}
	return server.ListenAndServe()
}

// Stop tries to shutdown running server, if any.
// Returns non-nil error if server does not exists.
func Stop() error {
	if server == nil {
		return errors.New("already stoped")
	}

	err := server.Shutdown(context.Background())
	server = nil
	return err
}

// Running retuns true when server is probably serving
func Running() bool {
	return server != nil
}

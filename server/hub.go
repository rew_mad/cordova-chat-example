// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package server

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	close chan struct{}
}

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
		close:      make(chan struct{}, 1),
	}
}

func (h *Hub) run() {
	defer func() {
		// close all clients
		for clinet, ok := range h.clients {
			if !ok {
				continue
			}

			close(clinet.send)
		}
	}()
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			// TODO: rewrite in more Goish way
			go func() {
				h.broadcast <- newJoinMsg(client.username)
			}()
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				go func() {
					h.broadcast <- newLeaveMsg(client.username)
				}()
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		case <-h.close:
			return
		}
	}
}

func (h *Hub) stop() {
	h.close <- struct{}{}
}

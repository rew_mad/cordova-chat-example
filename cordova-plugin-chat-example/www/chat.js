var exec = require('cordova/exec');
var channel = require('cordova/channel');

/**
 * @constructor
 */
function ChatServer() {
    this.available = false;
    this.status = "unknown";

    var self = this;
    channel.onCordovaReady.subscribe(function () {
        self.isRunning(function (flag) {
            self.available = true;
        }, function (e) {
            self.available = false;
        });
    });
}

ChatServer.prototype.start = function(successCallback, errorCallback, port) {
    // TODO: check server status before run it

    var self = this;
    exec(function (resp) {
        self.status = "stopped";
        successCallback(resp);
    }, function (error) {
        self.status = "failed";
        errorCallback(error);
    }, "ChatServer", "start", [port]);
    self.status = "listening";
};

ChatServer.prototype.stop = function(successCallback, errorCallback) {
    var self = this;
    exec(function (resp) {
        self.status = "shutdown";
        successCallback(resp);
    }, errorCallback, "ChatServer", "stop", []);
};

ChatServer.prototype.observeLogs = function(successCallback, errorCallback) {
    exec(successCallback, errorCallback, "ChatServer", "observeLogs");
};

ChatServer.prototype.stopObserveLogs = function(successCallback, errorCallback) {
    exec(successCallback, errorCallback, "ChatServer", "stopObserveLogs");
};

ChatServer.prototype.isRunning = function(successCallback, errorCallback) {
    var self = this;
    exec(function (flag) {
        self.status = flag ? "listening" : "stopped";
        successCallback(flag);
    }, errorCallback, "ChatServer", "isRunning");
};

module.exports = new ChatServer();

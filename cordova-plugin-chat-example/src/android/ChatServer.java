package io.rewmad.chatexample;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import server.Server;
import server.LogObserver;

public class ChatServer extends CordovaPlugin {
    private final ChatLogObserver _logObserver = new ChatLogObserver();

    public ChatServer() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like get
     * file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArray of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into
     *                        JavaScript.
     * @return True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        switch (action) {
        case "start": {
            int port = args.getInt(0);
            ChatServer self = this;
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    try {
                        Server.start(port, false, self._logObserver);
                        callbackContext.success();
                    } catch (Exception e) {
                        callbackContext.error(e.toString());
                    }
                }
            });
            return true;
        }

        case "stop": {
            try {
                Server.stop();
                callbackContext.success();
            } catch (Exception e) {
                callbackContext.error(e.toString());
            }
            return true;
        }

        case "observeLogs": {
            try {
                this._logObserver.setCallback(callbackContext);
            } catch (Exception e) {
                callbackContext.error(e.toString());
            }
            return true;
        }

        case "stopObserveLogs": {
            try {
                this._logObserver.clearCallback();
                callbackContext.success();
            } catch (Exception e) {
                callbackContext.error(e.toString());
            }
            return true;
        }

        case "isRunning": {
            callbackContext.success(Server.running() ? 1 : 0);
            return true;
        }
        }
        return false;
    }

    public void onDestroy() {
        try {
            Server.stop();
        } catch (Exception e) {
            // TODO: log this
        }
    }

    private class ChatLogObserver implements LogObserver {

        private CallbackContext _pendingCallback;
        private final Object _pendingCallbackLock = new Object();

        @Override
        public void next(String message) throws Exception {
            synchronized (this._pendingCallbackLock) {
                if (this._pendingCallback == null) {
                    throw new Exception("No callback provided");
                }

                JSONObject resultJson = new JSONObject();
                try {
                    resultJson.put("message", message);
                } catch ( Exception e ) {
                    resultJson.put("message", "Empty message" + e.getMessage());
                }
                PluginResult result = new PluginResult(PluginResult.Status.OK, resultJson);
                result.setKeepCallback(true);
                this._pendingCallback.sendPluginResult(result);
            }
        }

        public void setCallback(CallbackContext callback) {
            if (callback == null) {
                // TODO: orly?!
                return;
            }
            synchronized (this._pendingCallbackLock) {
                this._pendingCallback = callback;

                PluginResult result = new PluginResult(PluginResult.Status.OK, "callback subscribed");
                result.setKeepCallback(true);
                this._pendingCallback.sendPluginResult(result);
            }
        }

        public void clearCallback() {
            synchronized (this._pendingCallbackLock) {
                if (this._pendingCallback == null) {
                    // TODO: orly?!
                    return;
                }

                try {
                    PluginResult result = new PluginResult(PluginResult.Status.OK, "callback unsubscribed");
                    this._pendingCallback.sendPluginResult(result);
                } finally {
                    this._pendingCallback = null;
                }
            }
        }
    }
}
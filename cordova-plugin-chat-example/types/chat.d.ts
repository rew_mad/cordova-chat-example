declare namespace ChatExample {
    interface Server {
        available: boolean;
        status: 'unknown' | 'listening' | 'shutdown' | 'stopped' | 'failed';

        start(successCallback: () => void, errorCallback: (error: string) => void, port: number): void;
        stop(successCallback: () => void, errorCallback: (error: string) => void): void;
        observeLogs(successCallback: (result: 'callback subscribed' | { message: string } | 'callback unsubscribed') => void, errorCallback: (error: string) => void): void;
        stopObserveLogs(successCallback: () => void, errorCallback: (error: string) => void): void;
    }
}

interface Window {
    chat: ChatExample.Server;
}

declare var chat: ChatExample.Server;

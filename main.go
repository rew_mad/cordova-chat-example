// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/rew_mad/cordova-chat-example/server"
)

var port = flag.Int("port", 8080, "http service port")

func main() {
	flag.Parse()

	go func() {
		c := make(chan os.Signal, 2)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		<-c
		err := server.Stop()
		if err != nil {
			log.Print(err)
		}
	}()

	err := server.Start(*port, true, nil)
	if err != nil && err != http.ErrServerClosed {
		log.Fatal("ListenAndServe: ", err)
	}
}
